#ifndef DUNE_FEM_SPACE_FOURIER_DECLARATION_HH
#define DUNE_FEM_SPACE_FOURIER_DECLARATION_HH

namespace Dune
{

  namespace Fem
  {

    // FourierDiscreteFunctionSpace
    // ----------------------------

    template< class FunctionSpace, class GridPart, int order >
    class FourierDiscreteFunctionSpace;

  } // namespace Fem

} // namespace Dune

#endif // #ifndef DUNE_FEM_SPACE_FOURIER_DECLARATION_HH
